import { PropsWithChildren } from 'react'
import {
  Carousel,
  CarouselContent,
  CarouselNext,
  CarouselPrevious,
} from "@/components/ui/carousel"
import Autoplay from 'embla-carousel-autoplay'

interface ProductCarouselProps extends PropsWithChildren {
  className?: string
}

export default function ProductCarousel({ children, ...props }: ProductCarouselProps) {
  return (
    <Carousel {...props}
    opts={{
      loop: false,
    }}
    plugins={[
      Autoplay({
        delay: 3000,
      }),
    ]}>
      <CarouselContent>
        {children}
      </CarouselContent>
      <CarouselPrevious className='translate-x-8' />
      <CarouselNext className='-translate-x-8' />
    </Carousel>
  )
}
