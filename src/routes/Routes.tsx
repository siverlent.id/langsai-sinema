import App from "@/App";
import SignIn from "@/pages/auth/signIn/SignIn";
import SignUp from "@/pages/auth/signUp/SignUp";
import Homepage from "@/pages/home/homepage/Homepage";
import { createBrowserRouter } from "react-router-dom";

export const router = createBrowserRouter([
  {
    path: '/',
    element: <App />,
    children: [
      {path: '', element: <Homepage />},
      {path: 'sign-in', element: <SignIn />},
      {path: 'sign-up', element: <SignUp />},
    ]
  }
])