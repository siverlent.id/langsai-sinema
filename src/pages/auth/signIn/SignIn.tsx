import { Separator } from '@/components/ui/separator'
import AuthLayout from '../layouts/AuthLayout'
import Form from "./components/Form"

interface SignInProps {

}

export default function SignIn({ }: SignInProps) {
  return (
    <AuthLayout>
      <div className="mx-auto grid w-[350px] gap-6">
        <div className="grid gap-2 text-left">
          <h1 className="text-3xl font-bold">Login</h1>
          <p className="text-balance text-muted-foreground">
            Enter your email below to login to your account
          </p>
        </div>
        <Separator />
        <div className="grid gap-4 text-left">
          <Form />
        </div>
        <div className="mt-4 text-center text-sm">
          Don&apos;t have an account?{" "}
          <a href="/sign-up" className="underline">
            Sign up
          </a>
        </div>
      </div>
    </AuthLayout>
  )
}
