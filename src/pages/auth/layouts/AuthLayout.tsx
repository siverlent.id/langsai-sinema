import { buttonVariants } from '@/components/ui/button'
import { cn } from '@/lib/utils'
import { ArrowLeft } from 'lucide-react'
import React from 'react'

interface AuthLayoutProps extends React.PropsWithChildren{
  
}

export default function AuthLayout({children}: AuthLayoutProps) {
  return(
    <div className="w-full lg:grid lg:grid-cols-2 h-screen px-8 md:px-0">
      <a href="/" className={cn(buttonVariants({variant: 'outline'}), 'absolute top-5 right-5 bg-transparent md:text-white')}>
      <ArrowLeft className='h-4 w-4 me-1'/>
        Home
      </a>
        <div className="flex items-center justify-center py-12">
          {children}
        </div>
        <div className="hidden bg-muted lg:block">
          <img
            src="https://images.unsplash.com/photo-1577190651915-bf62d54d5b36?q=80&w=2069&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
            alt="Image"
            width="1920"
            height="1080"
            className="h-full w-full object-cover dark:brightness-[0.2] dark:grayscale"
          />
        </div>
      </div>
  )
}
