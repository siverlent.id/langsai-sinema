interface NavItemsMobileProps {

}

export default function NavItemsMobile({ }: NavItemsMobileProps) {
  return (
    <>
      <a
        href="/"
        className="flex items-center gap-2 text-lg font-semibold"
      >
        <span className="font-bold uppercase italic text-md">Langsai Sinema</span>
      </a>
      <a href="/" className="hover:text-foreground">
        Home
      </a>
      <a
        href="/equipment"
        className="text-muted-foreground hover:text-foreground"
      >
        Equipment
      </a>
      <a
        href="/featured"
        className="text-muted-foreground hover:text-foreground"
      >
        Featured
      </a>
      <a
        href="/info"
        className="text-muted-foreground hover:text-foreground"
      >
        Info
      </a>
    </>
  )
}
