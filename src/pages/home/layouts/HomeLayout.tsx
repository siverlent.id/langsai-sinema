import React from 'react'
import Navbar from '../components/Navbar'

interface HomeLayoutProps extends React.PropsWithChildren {
  
}

export default function HomeLayout({children}: HomeLayoutProps) {
  return(
    <>
      <Navbar />
      {children}
    </>
  )
}
