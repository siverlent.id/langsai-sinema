import FeaturedEquipmentItems from './FeaturedEquipmentItems'
import equipments from '@/pages/home/datas/equipments.json'
import { Separator } from '@/components/ui/separator'
import ProductCarousel from '@/components/ProductCarousel'
import { CarouselItem } from '@/components/ui/carousel'
import useIsMobile from '@/hooks/useIsMobile'
import { cn } from '@/lib/utils'
import { buttonVariants } from '@/components/ui/button'
import { ArrowRight } from 'lucide-react'

interface FeaturedEquipmentProps {
}

export default function FeaturedEquipment({ }: FeaturedEquipmentProps) {
  const isMobile = useIsMobile();
  return (
    <>
      {/* FE Header */}
      <div className='pt-10 flex flex-col md:flex-row md:justify-between items-start md:items-end space-y-4 md:space-y-0'>
          <div className='text-left md:w-1/2 space-y-2'>
            <h3 className='text-xl uppercase font-bold'>Featured Equipments</h3>
            <p className='text-muted-foreground text-xs md:text-sm'>Lorem ipsum dolor sit amet consectetur adipisicing elit. Optio libero harum iure saepe qui dolorem laboriosam. Blanditiis incidunt eveniet minima.</p>
          </div>
          <div>
            <a href="" className={cn(buttonVariants({ variant: 'ghost' }), 'space-x-4 p-0 hover:bg-transparent')}>
              <span className='font-normal text-xs md:text-sm uppercase'>
                Explore Equipments
              </span>
              <div className='p-1 md:p-2 border-gray-300 border rounded-full'>
                <ArrowRight className='w-3 md:w-4 h-3 md:h-4' />
              </div>
            </a>
          </div>
        </div>
      <Separator className='mt-4'/>
      {/* FE Items */}
      {!isMobile ?
        <div className="grid grid-cols-2 md:grid-cols-5 md:grid-rows-2 gap-4 mt-10">
          {equipments.map((item, index) => (
            <FeaturedEquipmentItems key={index} item={item} />
          ))}
          {equipments.map((item, index) => (
            <FeaturedEquipmentItems key={index} item={item} />
          ))}
        </div> :
        <ProductCarousel className="mt-10">
          {equipments.map((item, index) => (
            <CarouselItem>
              <FeaturedEquipmentItems key={index} item={item} />
            </CarouselItem>
          ))}
        </ProductCarousel>}
    </>
  )
}
