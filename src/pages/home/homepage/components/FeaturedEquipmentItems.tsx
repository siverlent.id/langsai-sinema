import { buttonVariants } from "@/components/ui/button";
import {
  Card,
  CardContent,
  CardDescription,
  CardFooter,
  CardHeader,
  CardTitle,
} from "@/components/ui/card"
import { Badge } from "@/components/ui/badge"
import { cn } from "@/lib/utils";
import { makeEllipsis } from "@/lib/truncateText";

interface FeaturedEquipmentItemsProps {
  item: {
    name: string;
    description: string;
    imgUrl: string;
    price: number;
    tag: string;
  };
}

const formatPrice = (amount: number): string => {
  return Intl.NumberFormat('id-ID', {
      style: 'currency',
      currency: 'IDR'
  }).format(amount);
}

export default function FeaturedEquipmentItems({ item }: FeaturedEquipmentItemsProps) {
  return (
    <>
      <Card className="rounded">
        <CardContent className="relative pt-5">
          <div className="flex justify-start items-center">
            <Badge className="rounded-lg leading-6 bg-secondary text-gray-800 text-xs">{item.tag}</Badge>
          </div>
          <img src={item.imgUrl} alt={item.name} className="w-full md:w-full h-auto" />
        </CardContent>
        <CardHeader className="text-left pt-0">
          <CardTitle className="leading-5">{item.name}</CardTitle>
          <p className="text-sm font-semibold">{formatPrice(item.price)}</p>
          <CardDescription className="text-xs">{makeEllipsis(item.description, 100)}</CardDescription>
        </CardHeader>
        <CardFooter>
          <div className="space-y-4">
          <a href="" className={cn(buttonVariants({ variant: 'default' }), 'rounded text-xs')}>Book Now</a>
          </div>
        </CardFooter>
      </Card>
    </>
  )
}
