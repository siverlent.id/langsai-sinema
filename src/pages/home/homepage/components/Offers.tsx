import offers from '@/pages/home/datas/offers.json'
interface OffersProps {

}

const Offers = ({ }: OffersProps) => {
  return (
    <>
    <div className="pt-10">
        <div className="h-max mx-auto w-full md:w-[600px] lg:w-[1200px] flex flex-col items-center px-6 md:px-0 space-y-10">
          <div className='text-center md:w-[400px] space-y-2'>
          <h3 className='text-base uppercase font-bold'>Langsai Sinema Offers</h3>
            <p className='text-muted-foreground text-xs md:text-sm'>Lorem ipsum dolor sit amet consectetur adipisicing elit. Optio libero harum iure saepe qui dolorem laboriosam. Blanditiis incidunt eveniet minima.</p>
          </div>
          <div className="w-full"></div>
        </div>
      </div>
      <div className="flex flex-col md:flex-row">
      {offers.map((item) => (
        <div className='relative flex-none md:w-1/2'>
          <img src={item.imgUrl} alt="" className=' relative h-[400px] w-full object-cover' />
          <div className='absolute bottom-10 md:bottom-20 left-10 md:left-20 text-left text-gray-100 uppercase space-y-2'>
            <p className='text-sm md:text-base'>{item.subTitle}</p>
            <h3 className='text-2xl font-semibold pt-0 mt-0'>{item.title}</h3>
            <p className='text-xs md:text-sm'>{item.description}</p>
          </div>
        </div>
      ))}
      </div>
    </>
  )
}

export default Offers
