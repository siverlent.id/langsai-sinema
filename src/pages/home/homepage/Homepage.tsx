import {
  Carousel,
  CarouselContent,
  CarouselItem,
  CarouselNext,
  CarouselPrevious,
} from "@/components/ui/carousel"
import HomeLayout from '../layouts/HomeLayout'
import FeaturedEquipment from "./components/FeaturedEquipment"
import Autoplay from 'embla-carousel-autoplay';
import categories from '@/pages/home/datas/categories.json'
import { makeEllipsis } from "@/lib/truncateText";
import { buttonVariants } from "@/components/ui/button";
import { cn } from "@/lib/utils";
import { ArrowRight } from "lucide-react";
import { Separator } from "@/components/ui/separator";
import Offers from "./components/Offers";


interface HomepageProps {

}

export default function Homepage({ }: HomepageProps) {
  return (
    <HomeLayout>
      <div className="relative h-[35rem]">
        <div className="absolute top-1/2 left-1/2 -translate-x-1/2 md:-translate-x-[80%] -translate-y-1/2 w-full md:w-[600px] text-left text-white space-y-4 md:space-y-4 px-4 md:px-0 z-10">
          <div className="space-y-2 md:space-y-4">
            <h1 className="text-3xl md:text-6xl font-semibold">Production Equipments</h1>
            <h2 className="text-xl md:text-4xl font-semibold">Trusted by 5000+ Customers</h2>
            <p className="text-sm md:text-lg">We make sure to meet photographer & filmmaker needs <br />
              Located in Bali, Bandung, Surabaya & Semarang <br />
              Get Free Delivery & Extra 1 day for 2 days rent</p>
          </div>
          <div>
            <a href="" className={cn(buttonVariants({ variant: 'outline' }), 'bg-transparent text-xs md:text-sm')}>
              Explore Now
              <ArrowRight className='w-4 h-4 ms-2 p-0 m-0' />
            </a>
          </div>
        </div>
        <Carousel className="z-0">
          <CarouselContent className="h-[35rem] brightness-50 z-0">
            <CarouselItem>
              <img
                className="w-full h-full object-cover"
                width="1920"
                height="1080"
                src="/img/hero.jpg"
                alt="slider-2"
              />
            </CarouselItem>
          </CarouselContent>
        </Carousel>
      </div>

      {/* Categories */}
      <div className="flex flex-col w-full md:w-[600px] lg:w-[1200px] mx-auto px-6 md:px-0 pb-10 space-y-4">
        <div className='pt-10 flex flex-col md:flex-row md:justify-between items-start md:items-end space-y-4 md:space-y-0'>
          <div className='text-left md:w-1/2 space-y-2'>
            <h3 className='text-xl uppercase font-bold'>Categories</h3>
            <p className='text-muted-foreground text-xs md:text-sm'>Lorem ipsum dolor sit amet consectetur adipisicing elit. Optio libero harum iure saepe qui dolorem laboriosam. Blanditiis incidunt eveniet minima.</p>
          </div>
          <div>
            <a href="" className={cn(buttonVariants({ variant: 'ghost' }), 'space-x-4 p-0 hover:bg-transparent')}>
              <span className='font-normal text-xs md:text-sm uppercase'>
                All Categories
              </span>
              <div className='p-1 md:p-2 border-gray-300 border rounded-full'>
                <ArrowRight className='w-3 md:w-4 h-3 md:h-4' />
              </div>
            </a>
          </div>
        </div>
        <Separator className='mt-4' />
        <Carousel
          opts={{
            loop: false,
          }}
          plugins={[
            Autoplay({
              delay: 3000,
            }),
          ]}
          className="w-full pt-4"
        >
          <CarouselContent className="-ml-1">
            {categories.map((item, index) => (
              <CarouselItem key={index} className={`relative h-[450px] md:h-[600px] pl-1 md:basis-1/2 lg:basis-1/3`}>
                <img src={item.imgUrl} className="w-full h-full object-cover" />
                <div className="absolute bottom-0 h-1/3 md:h-1/4 bg-black/80">
                  <div className="text-secondary text-left p-6 space-y-2">
                    <h2 className="uppercase tracking-widest font-bold">{item.name}</h2>
                    <p className="text-xs">{makeEllipsis(item.description, 180)}</p>
                  </div>
                </div>
              </CarouselItem>
            ))}
          </CarouselContent>
          <CarouselPrevious className='translate-x-8' />
          <CarouselNext className='-translate-x-8' />
        </Carousel>
      </div>

      {/* Featured Equipment */}
      <div className="flex flex-col w-full md:w-[600px] lg:w-[1200px] mx-auto px-6 md:px-0 pb-10">
        <FeaturedEquipment />
      </div>

      {/* Customers Feeddback */}
      <div className="bg-gray-50 py-10">
        <div className="h-max w-full md:w-[600px] lg:w-[1200px] flex flex-col md:flex-row items-start mx-auto px-6 md:px-0 py-10 space-y-10 md:space-y-0 md:space-x-20">
          <div className='text-center md:text-left md:w-[400px] flex-none space-y-2'>
            <h3 className='text-base uppercase font-bold'>Customers Feedback</h3>
            <p className='text-muted-foreground text-xs md:text-sm'>Lorem ipsum dolor sit amet consectetur adipisicing elit. Optio libero harum iure saepe qui dolorem laboriosam. Blanditiis incidunt eveniet minima.</p>
          </div>
          <div className="w-full">
            <Carousel
              opts={{
                loop: false,
              }}
              plugins={[
                Autoplay({
                  delay: 3000,
                }),
              ]}
              className="w-full pt-4 flex flex-col"
            >
              <CarouselContent className="-ml-1 w-full">
                {categories.map((item, index) => (
                  <CarouselItem key={index} className={`relative pl-1 me-10 md:w-0 md:basis-[20rem]`}>
                    <div className="flex flex-col space-y-4">
                      <div className="flex items-center space-x-5">
                        <img src={item.imgUrl} alt="" className="w-16 h-16 rounded-full object-cover" />
                        <div className="text-left">
                          <h5>{item.name}</h5>
                          <p className="text-xs italic">Movie Maker</p>
                        </div>
                      </div>
                      <div className="text-left text-sm text-muted-foreground">
                        <p>{makeEllipsis(item.description, 170)}</p>
                      </div>
                    </div>
                    {/* <img src={item.imgUrl} className="w-full h-full object-cover" />
                    <div className="absolute bottom-0 h-1/3 md:h-1/4 bg-black/80">
                      <div className="text-secondary text-left p-6 space-y-2">
                        <h2 className="uppercase tracking-widest font-bold">{item.name}</h2>
                        <p className="text-xs">{makeEllipsis(item.description, 180)}</p>
                      </div>
                    </div> */}
                  </CarouselItem>
                ))}
              </CarouselContent>
              <div className="space-x-4 flex flex-row items-center justify-end mt-6 md:mt-10">
                <CarouselPrevious className='relative translate-x-0 translate-y-0 left-0' />
                <CarouselNext className='relative -translate-x-0 translate-y-0 left-0' />
              </div>
            </Carousel>
          </div>
        </div>
      </div>

      {/* Offers */}
      <Offers />

      {/* Brands */}
      <div className="py-10">
        <div className="h-max mx-auto w-full md:w-[600px] lg:w-[1200px] flex flex-col items-center px-6 md:px-0 py-10 space-y-10">
          <div className='text-center md:w-[400px] space-y-2'>
            <h3 className='text-base uppercase font-bold text-muted-foreground'>Brands</h3>
          </div>
          <div className="w-full"></div>
        </div>
      </div>
    </HomeLayout>
  )
}
